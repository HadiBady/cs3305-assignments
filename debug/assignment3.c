/** Developed by Hadi Badawi
	purpose of this program is to simulate the different process queues in C
*/

#include<stdlib.h>
#include<stdio.h>
#include<ctype.h>
#include<string.h>
#include<pthread.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/types.h>

int accCreate = 0;
int depos = 0;
int cli = 0;
int lineNum =0;

int cliLock = 0;

pthread_mutex_t lock;

typedef struct transaction {
	int position;
	char * string;
	int length;
	int id;
};


// created this structure to hold the process information
typedef struct account {
	int id; // which identifies the the process ID
	char* type;
	int balance;	// holds the burstTime of each process
	int depositFee;
	int withdrawFee;
	int transferFunds;
	int transactionCount;
	int transactionLimit;
	int transactionFee;
	int overdraft;
	int overdraftFee;
};

struct account ** accountCreation;
struct transaction ** depositors;
struct transaction ** clients;



int lineNumber = 0;	// holds the number of lines for the input file

/**
	checkTransactionCount determines to charge a fee or not depending on the fee of going over the transaction
	amount
*/
void checkTransactionCount(struct account* tempA){
	if (tempA->transactionCount + 1 == tempA->transactionLimit){
		tempA->balance -= tempA->transactionFee;
	}
}

char getChar(FILE* file, char tempChar){
	fscanf(file, "%c", &tempChar);
	while(tempChar == '\n' || tempChar == ' ' || tempChar == '\000')
		fscanf(file, "%c", &tempChar);

	return tempChar;
}



/**
	

*/

void depositFun (struct account *tempAccount, int amount, int *typeAcc){
	pthread_mutex_lock(&lock);

	while (*typeAcc == 1);

	checkTransactionCount(tempAccount);
	tempAccount->balance += amount;
	tempAccount->transactionCount += 1;

	pthread_mutex_unlock(&lock);
}


void transferFun (struct account *tempA1, struct account *tempA2, int amount, int * typeAcc){

	pthread_mutex_lock(&lock);

	while (*typeAcc == 1);

	tempA1->balance -= amount;
	checkTransactionCount(tempA1);
	tempA1->transactionCount +=1;
	checkTransactionCount(tempA2);
	tempA2->balance += amount;
	tempA2->transactionCount +=1;

	pthread_mutex_unlock(&lock);

}

void withdrawFun (struct account * tempA, int amount, int * typeAcc){
	pthread_mutex_lock(&lock);

	while (*typeAcc == 1);

	checkTransactionCount(tempA);
	tempA->balance -=amount;

	tempA->transactionCount +=1;
	pthread_mutex_unlock(&lock);

}

/**
	overdraft function calculates the overdraft fee according to the situation

*/
void overdraft(struct account tempA){

	while (tempA.overdraft == 1 ){
		if (tempA.balance >= -500 && tempA.balance <= -1 && tempA.balance-tempA.overdraftFee >= -5000){
			tempA.balance -= tempA.overdraftFee;
		}
		if (tempA.balance >= -1000 && tempA.balance <= -501 &&  tempA.balance-tempA.overdraftFee >= -5000){
			tempA.balance -= tempA.overdraftFee;
		}
		if (tempA.balance >= -1500 && tempA.balance <= -1001 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
		if (tempA.balance >= -2000 && tempA.balance <= -1501 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
		if (tempA.balance >= -2500 && tempA.balance <= -2001 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
		if (tempA.balance >= -3000 && tempA.balance <= -2501 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
		if (tempA.balance >= -3500 && tempA.balance <= -3001 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
		if (tempA.balance >= -4000 && tempA.balance <= -3501 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
		if (tempA.balance >= -4500 && tempA.balance <= -4001 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
		if (tempA.balance >= -5000 && tempA.balance <= -4501 &&  tempA.balance-tempA.overdraftFee >= -5000){
                        tempA.balance -= tempA.overdraftFee;
                }
	}

}

/**
	openFile function opens a file, creates the proper structures and stores them in the queueTable
*/
void openFile(char* filename){
	FILE * file; // pointer which will be used for input file

	file = fopen(filename, "r");
	ssize_t lineCharNum = 0;
	char *line = NULL;
	ssize_t read;
	size_t len = 0;
	

	// this while loop  counts the number of lines in a file, which represents the number of queues in the file
	while(read = getline(&line, &len, file) != -1){
		if (line[0] == 'a'){
			accCreate++;
		}
		else if(line[0] == 'd'){
			depos++;
		}
		else if (line[0] == 'c'){
			cli++;
		}
		if (lineCharNum < len){
			lineCharNum = len;} // this stores the number of characters each line stores
		lineNum++;
	}

	accountCreation = (struct account **) malloc(accCreate * sizeof(struct account*)); // we are allocating space for the queues to be stored
	depositors = (struct transaction **) malloc(depos *sizeof(struct transaction *));
	clients = (struct transaction **) malloc(cli * sizeof(struct transaction *));

	
	// rewind the file back to the beginning to actually being storing the processes in the correct queues
	rewind(file);

	// this temp string will be used to store tq to read it when read it from the file
	char* tempString = malloc(sizeof(char)*256);
	char tempChar; // this will be used to read characters from the file
	int tempDigit; // this will store the temporary integer value from file

	//int track=0;
	int accCre = 0;
	int dep = 0;
	int cl = 0;


	int first = 1;		// this is supposed to signifiy that if the value is the first to be added to queue
	int currentLine = 0;	// this stores that currentLine which we are reading from the file

	// this while loop will read the file and store the corresponding information into the queueTable
	while(currentLine != lineNum) {
		//fscanf(file, "%c", &tempChar);
		tempChar = getChar(file, tempChar);
		if (tempChar == 'a') {
			// if (read = getline(&line, &len, file) != -1){
//			strcat(tempStr, line);
			// maybe read line than do a scanf since it works on any char *
			accountCreation[accCre] = malloc(sizeof(struct account));
			accountCreation[accCre]->type = malloc(sizeof(256));
			fscanf(file, "%d", &tempDigit);
			accountCreation[accCre]->id = tempDigit;
			fscanf(file, "%s", tempString);
			fscanf(file, "%s", tempString);
			strcpy(accountCreation[accCre]->type,tempString);
			//fscanf(file, "%c", &tempChar);
			tempChar = getChar(file, tempChar);
			fscanf(file, "%d", &tempDigit);
			accountCreation[accCre]->depositFee = tempDigit;
			//fscanf(file, "%c", &tempChar);
			tempChar = getChar(file, tempChar);
			fscanf(file, "%d", &tempDigit);
			accountCreation[accCre]->withdrawFee = tempDigit;
			//fscanf(file, "%c", &tempChar);
			tempChar = getChar(file, tempChar);
			fscanf(file, "%d", &tempDigit);
			accountCreation[accCre]->transferFunds=tempDigit;
			fscanf(file, "%s", tempString);
			//tempChar = getChar(file, tempChar);
			fscanf(file, "%d", &tempDigit);
			accountCreation[accCre]->transactionCount = tempDigit;
			fscanf(file, "%d", &tempDigit); 
			accountCreation[accCre]->transactionLimit = tempDigit;
			fscanf(file, "%s", tempString);
			//fscanf(file, "%c", &tempChar);
			tempChar = getChar(file, tempChar);
			if (tempChar == 'Y'){
				accountCreation[accCre]->overdraft = 1;
				fscanf(file, "%d", &tempDigit);
				accountCreation[accCre]->overdraftFee = tempDigit;
			}
			else if (tempChar == 'N'){
				accountCreation[accCre]->overdraft = 0;}
			else{
				accountCreation[accCre]->overdraft = -1;}
			accountCreation[accCre]->balance = 0;

			accCre++;
			fscanf(file, "%c", &tempDigit);
		}
		else if (tempChar == 'd'){
			char tempStr[1024];
			strcpy(tempStr, " ");
			tempStr[0]=tempChar;
			if (read = getline(&line, &len, file) != -1){
				strcat(tempStr, line);
			}
			depositors[dep] = malloc(sizeof(struct transaction));
			depositors[dep]->position = 0;
			depositors[dep]->length = len;
			depositors[dep]->id = dep+1;
			depositors[dep]->string = malloc(sizeof(char)*len);
			strcpy(depositors[dep]->string, tempStr);
			printf("%s", depositors[dep]->string);
			dep++;
			//scanf("%s", tempString);
		}
		else if (tempChar == 'c'){
			char tempStr[1024];
			strcpy(tempStr, " ");
			tempStr[0]=tempChar;
			if (read = getline(&line, &len, file) != -1){
				strcat(tempStr, line);}
			clients[cl] = malloc(sizeof(struct transaction));
			clients[cl]->position = 0;
			clients[cl]->length = len;
			clients[cl]->id = cl+1;
			clients[cl]->string = malloc(sizeof(char)*len);
			strcpy(clients[cl]->string, tempStr);
			cl++;
		}
		first = 1;	//  reseting the value of first
		currentLine++;	// updating the currentLine value
	}
    fclose(file); // closing the file after the work is done
}


int returnNumFromString(char * line, int * position){
	int counter;
	char isDigit = line[*position];
	while (isdigit(isDigit)){
		counter++;
	}
	char tempNum[counter];
	strcpy(tempNum, (line + *position + counter-1));
	int id = atoi(tempNum);
	(*position)++;
	return id;
}

struct account* getAccount(int accID){
	for (int x =0; x < accCreate; x++){
		if (accountCreation[x]->id == accID){
			return accountCreation[x];
		}
	}
	return NULL;
}

void *depositors_thread( struct transaction *tempTrans){
	char * tempStr[256];
	printf("%s\n",tempTrans->string);
	strcpy(tempStr, tempTrans->string);
	char temps[256];
	int * tempDig;
	int * length;
	sprintf(temps, "%d" ,tempTrans->id);
	tempTrans->position = 2+strlen(temps)+2; //(starting at location 0)
	*tempDig = tempTrans->position;
	*length = tempTrans->length;
	while ( *tempDig != *length){
		(*tempDig) += 3;
		int account_id = returnNumFromString(tempStr, tempDig);
		struct account * tempAcc = getAccount(account_id);
		if (tempAcc == NULL) {
			printf("Account %d does not exist", account_id);
			exit(1);
		}
		int account_id2 = returnNumFromString(tempStr, tempDig);
		struct account * tempAcc2 = getAccount(account_id2);
		(*tempDig)++;
		int ammount = returnNumFromString(tempStr, tempDig);
		depositFun(tempAcc,  ammount,0);
	}
}



void *client_thread(struct transaction *tempTrans){
	char * tempStr[256];
	printf("%s\n", tempTrans->string);
	strcpy(tempStr,tempTrans->string);
	char temps[256];
	int * tempDig;
	int * length;
	sprintf(temps, "%d", tempTrans->id);
	//itoa(accCreate, tLeng, 10);
	tempTrans->position = 2+strlen(temps)+1; //(starting at location 0)
	*tempDig = tempTrans->position;
	*length = tempTrans->length;
	char tempChar;
	while ( *tempDig != *length){
		if (tempStr[*tempDig] == 'd' || tempStr[*tempDig] == 'w'){
		tempChar = tempStr[*tempDig];
		(*tempDig) += 3;
		int account_id = returnNumFromString(tempStr, tempDig);
		struct account * tempAcc = getAccount(account_id);
		if (tempAcc == NULL) {
			printf("Account %d does not exist\n", account_id);
			exit(1);
		}
		(*tempDig)++;

		int ammount = returnNumFromString(tempStr, tempDig);
		if (tempChar == 'd'){
			depositFun(tempAcc, ammount,&cliLock);
		}
		else {
			withdrawFun(tempAcc, ammount,&cliLock);

		}

		}

		else if (tempStr[*tempDig] == 't'){
			(*tempDig) += 3;
			int account_id = returnNumFromString(tempStr, tempDig);
			struct account * tempAcc = getAccount(account_id);
			if (tempAcc == NULL) {
				printf("Account %d does not exist\n", account_id);
				exit(1);
			}
			int account_id2 = returnNumFromString(tempStr, tempDig);
			struct account * tempAcc2 = getAccount(account_id2);
			if (tempAcc2 == NULL){
				printf("Account %d does not exit\n",account_id2);
			}
			(*tempDig)++;
			int ammount = returnNumFromString(tempStr, tempDig);
			transferFun(tempAcc, tempAcc2,  ammount,&cliLock);

		}
	}

}


/**

	the main function performs the simulation of the cpu scheduling algorithms and prints it to the screen
*/
int main(int argc, char* argv){
	// clearing the file from previous work by opeing it for write and closing it
	char *outfile = "assignment_3_output_file.txt";
	FILE * file = fopen("assignment_3_output_file.txt", "w");
	fclose(file);

	// setting up the queueTable with the openFile funciton
	openFile("assignment_3_input_file.txt");


	pthread_t threads_depos[depos];
	pthread_t threads_cli[cli];

	// going through a while loop and having each queue go through each algorithm and storing it to the file
//	for(int x =0; x < depos; x++){
	int x = 0;
	printf("%p this is the depo thred\n", depositors[x]);
	printf("%s this is the string in the structure\n", depositors[x]->string);
	printf("%d this is the id\n", depositors[x]->id);

	pthread_create(&threads_depos[x], NULL, &depositors_thread, depositors[x]);
//	}
int y = 0;
//	for(int y=0; y < cli; y++){
//	pthread_create(&threads_cli[y], NULL, &client_thread, &threads_cli[y]);
//	}
	int i = 0;
//	for(int i = 0; i <depos; i++){
	pthread_join(threads_depos[i], NULL);
//	}

//	cliLock = 0;

//	for (int i =0; i<cli; i++){
//	pthread_join(threads_cli[i], NULL);
//	}

//	depositFun(accountCreation[0], 500, 1);

	FILE * fp = fopen(outfile, "w");

	for (int i =0 ; i < accCreate; i++){
		fprintf(fp, "a%d type %s %d\n", accountCreation[i]->id, accountCreation[i]->type, accountCreation[i]->balance);
	}
}
