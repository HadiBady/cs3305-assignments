/** Developed by Hadi Badawi
	purpose of this program is to simulate the different process queues in C
*/

#include<stdlib.h>
#include<stdio.h>
#include<ctype.h>
#include<string.h>


// created this structure to hold the process information
typedef struct proc {
	int id; // which identifies the the process ID
	int burstTime;	// holds the burstTime of each process
}proc;

// created this structure to hold the queue informaiton
typedef struct que{
	int id; // the id of the process queue
	int timeQuantum; // the time quantium value for the queue
	int proccessCount;	// how many processes are in the queue
	int longestProc;	// stores the longest process in the queue
	struct proc *arr;	// holds an array of stuff in the queue
}que;

// created this structure to hold information for each process in the Round Robin queue
typedef struct RRque{
	int setup; // whether the value was initialized or not
	int procID; // the process ID
	int burstTimeRemain; // how much burst time for each process remains
	int start; // at what time the process was executed
	int finish; // when the process finished execution
	int isFinished; // if the process if complete or not
}RRque;


int lineNumber = 0;	// holds the number of lines for the input file
struct que** queueTable;	// this double pointer will point to the queueTable, which is an array of queue structures
char * outfile = "cpu_scheduling_output_file.txt"; // storing the output file name to store the information of the simulation

/**
	openFile function opens a file, creates the proper structures and stores them in the queueTable
*/
void openFile(char* filename){
	FILE * file; // pointer which will be used for input file

	file = fopen(filename, "r");
	ssize_t lineCharNum = 0;
	char *line = NULL;
	ssize_t read;
	size_t len = 0;

	// this while loop  counts the number of lines in a file, which represents the number of queues in the file
	while(read = getline(&line, &len, file) != -1){
		lineNumber++;
		if (lineCharNum < len)
			lineCharNum = len; // this stores the number of characters each line stores
	}

	queueTable = (struct que**) malloc(lineNumber * sizeof(struct que*)); // we are allocating space for the queues to be stored
	struct que** q = queueTable; // copied the value of pointer to queueTable to q to modify and store structs into it

	// this for loop we allocate space for an array of process structures and a queue which will take that array's pointer and store it into it
	// then take that pointer and store it at queueTable in the appropriate index of queueTable
	for (int i = 0; i < lineNumber; i++){
		struct proc* tempArray = (struct proc*) malloc(lineCharNum*sizeof(struct proc));
		struct que* tempQueue = malloc(sizeof(struct que));
		tempQueue->id = i+1;
		tempQueue->arr = tempArray;
		*q = tempQueue;
		q++;
	}

	// rewind the file back to the beginning to actually being storing the processes in the correct queues
	rewind(file);

	// this temp string will be used to store tq to read it when read it from the file
	char* tempString = malloc(sizeof(char)*3);
	char tempChar; // this will be used to read characters from the file
	int tempDigit; // this will store the temporary integer value from file

	//int track=0;

	int first = 1;		// this is supposed to signifiy that if the value is the first to be added to queue
	int currentLine = 0;	// this stores that currentLine which we are reading from the file

	// this while loop will read the file and store the corresponding information into the queueTable
	while(currentLine != lineNumber) {
		fscanf(file,"%c", &tempChar); // reading for 'q'
		fscanf(file,"%d", &tempDigit);// reading for queue number, which we can discard due to using currentLine
		fscanf(file,"%s", tempString);// we are reading for string "tq"

		int comp = strcmp(tempString,"tq"); // if the strings match then we get a zero back then we know the strings are equal

		// we make sure that the strings are equal to now be able to read the time quantum
		if (comp == 0) {
			if (fscanf(file,"%d", &tempDigit) == 1){
				queueTable[currentLine]->timeQuantum = tempDigit;
			}
		}

		// a while loop to store in the proccesses into it and break out when we read end of line
		while(fscanf(file,"%c", &tempChar) == 1 && tempChar != '\n'){
			// check to make sure that the character we read is p so we add that process
			if (fscanf(file,"%c", &tempChar) == 1 && tempChar == 'p'){
				fscanf(file,"%d", &tempDigit); // reading in ID
				queueTable[currentLine]->arr[tempDigit-1].id = tempDigit;	// storing id into process
				int tempID = tempDigit;	// keeping a copy of the id
				queueTable[currentLine]->proccessCount = tempID; // we store the process ID to ensure we get max number of processes in queue
				fscanf(file,"%d", &tempDigit); // reading in the burst time
				queueTable[currentLine]->arr[tempID-1].burstTime = tempDigit; // storing in burst time
				if ( first || queueTable[currentLine]->longestProc < tempDigit){ // updating the longest process recorded if the first process or an actual long process
					queueTable[currentLine]->longestProc = tempDigit;
					first = 0; // dereferencing it is first
				}
			}
		}
		first = 1;	//  reseting the value of first
		currentLine++;	// updating the currentLine value
	}
    fclose(file); // closing the file after the work is done
}

/**
	FCFS scheduling takes in a queue and tries to perform the Ready Queue
*/
void FCFS(struct que *tempQue){
	// opening the file but also adding to it to not wipe out any previous work by the other function calls
	FILE * fp = fopen(outfile, "a+");

	// this process pointer points to an array of processes which we will store to keep track the order of execution
	struct proc * tempProc = malloc(sizeof(struct proc *) * tempQue->proccessCount);

	fprintf(fp, "Ready Queue %d Applying FCFS Scheduling:\n\n", tempQue->id);

	fprintf(fp, "Order of selection by CPU:\n", tempQue->id);

	// printing the processes in order of execution
	for( int x = 0; x < tempQue->proccessCount; x++){
		tempProc[x] = tempQue->arr[x];
		if(x != tempQue->proccessCount -1)
			fprintf(fp, "p%d ", x+1);
		else
			fprintf(fp, "p%d\n", x+1);
	}

	fprintf(fp, "\nIndividual waiting times for each process:\n");

	// these variables will be used to print the current waiting time, and calcualte average waiting time
	int currentWait = 0;
	int totalWait = 0;

	// loop goes through each item and prints the current waiting time and increments totalWait
	for(int y = 0; y< tempQue->proccessCount; y++){
		fprintf(fp, "p%d = %d\n", tempProc[y].id, currentWait);
		currentWait += tempProc[y].burstTime;
		totalWait += currentWait;
	}

	// calculating the average waiting time
	float aveWait = (float) totalWait / (float) tempQue->proccessCount;
	fprintf(fp, "\nAverage waiting time = %.2f\n\n", aveWait);

	// closing the file after finished
	fclose(fp);


}

/**
	 the SJF function simply simulates the SJF scheduling, by storing in order of execution the shortest jobs first
	then it does the similar calculuation as FCFS function

*/
void SJF(struct que *tempQue){
	FILE * fp = fopen(outfile, "a+");

	struct proc * tempProc = malloc(sizeof(struct proc) * tempQue->proccessCount); // this array holds the order of processes for this scheduling algorithm

	int tempCheckedProc[tempQue->proccessCount]; // this array holds whether each value was checked or not

	fprintf(fp, "Ready Queue %d Applying SJF Scheduling:\n\n", tempQue->id);

	fprintf(fp, "Order of selection by CPU:\n", tempQue->id);

	// here we have extra variables to ensure that we only add the values inorder
	// of which the values are increasing burst time
	int currentProc = 1;
	int currentVal = 0;
	int currentPos = 0;
	int potentialPos = 0;

	// this while loop will put items in the correct order
	while (potentialPos != tempQue->proccessCount){
		// reseting the values to be used again
		// since this while loop only exits when
		// all processes have been added in proper order
		currentVal = 0;
		char start = 's';
		int x = 0;
		// this while loop goes though all the values in the tempQue
		// and checks for the one which has not been already added
		// and if it is smaller then the current smallest value
		for(; x < tempQue->proccessCount; x++){
			// checks if this value has already been added or not
			if (tempCheckedProc[x] == 1)
				continue;
			// takes the first burst time and makes it the smallest value
			if(start == 's') {
				currentVal = tempQue->arr[x].burstTime;
				currentPos = x;
				start = 'f';
				continue;
			}
			// if there is a smaller value then we use that one instead
			if (tempQue->arr[x].burstTime < currentVal){
				currentPos = x;
				currentVal = tempQue->arr[x].burstTime;
			}
		}
		// here we add in the values  to the tempPrco in order of increasing burst time
		tempProc[potentialPos].id = tempQue->arr[currentPos].id;
		tempProc[potentialPos].burstTime = tempQue->arr[currentPos].burstTime;
		tempCheckedProc[currentPos] = 1; // we set it to 1 to ensure it is not added again
		potentialPos++;
	}

	// this segment is very similar to FCFS code
	fprintf(fp, "\nIndividual waiting times for each process:\n");
	int currentWait = 0;
	int totalWait = 0;

	for(int y = 0; y< tempQue->proccessCount; y++){
		fprintf(fp, "p%d = %d\n", tempProc[y].id, currentWait);
		currentWait += tempProc[y].burstTime;
		totalWait += currentWait;
	}

	float aveWait = (float) totalWait / (float) tempQue->proccessCount;
	fprintf(fp, "\nAverage waiting time = %.2f\n\n", aveWait);

	fclose(fp);


}

/**
	RR function implements the round robin algorithm
*/
void RR(struct que *tempQue){

	// opeing the out file to write ot the file the Round Robin scheduling result
	FILE * fp = fopen(outfile, "a+");

	// will store the current process we are dealing with
	int currentProc = 0;

	// this array holds in the processed  that have been completed and the order in which they were competed
	struct RRque* tempRRque = malloc(sizeof(struct RRque) * tempQue->proccessCount);

	// this holds in the processes while being processed by round robin
	struct RRque* tempRRque2 = malloc(sizeof(struct RRque) * tempQue->proccessCount);

	// this holds in the order of processes as they are executed
	struct proc * tempProc = malloc(sizeof(struct proc) * tempQue->proccessCount * tempQue->longestProc);

	fprintf(fp, "Ready Queue %d Applying RR Scheduling:\n\n", tempQue->id);

	fprintf(fp, "Order of selection by CPU:\n", tempQue->id);

	// processed process variable to count number of processess that completed execution
	int processesProcessed = 0;
	int currentTime = 0;	// this will hold the time before the process was executed
	int currentPos = 0;	// the current position in the tempProc array
	int timeToUse = 0;	// this is the time of which the process took to execute

	// while loop that goes through round robin and simulates the scheduling
	while (processesProcessed != tempQue->proccessCount){
		// goes through every value in the tempQue until all processes have been processed
		for(int x = 0; x < tempQue->proccessCount; x++){
			// check to make sure to not have the process enter back into execution
			if(tempRRque2[x].isFinished == 1)
				continue;
			// checks to see if the process was initalized for round robin
			if (tempRRque2[x].setup != 1 ){
				tempRRque2[x].procID = x+1;
				tempRRque2[x].setup = 1;
				tempRRque2[x].finish = 0;
				tempRRque2[x].isFinished = 0;
				tempRRque2[x].burstTimeRemain = tempQue->arr[x].burstTime;
				tempRRque2[x].start = currentTime;
			}
			// does the final execution where we store the final exection of the process
			// and we add it to the processed proccess queue and we copy the contents
			// to another queue which will have it stored in te order of execution
			if (tempRRque2[x].burstTimeRemain <= tempQue->timeQuantum) {
				tempRRque2[x].finish = tempRRque2[x].burstTimeRemain + currentTime;
				tempRRque2[x].isFinished = 1;
				tempProc[currentPos].id = tempQue->arr[x].id;
				tempProc[currentPos].burstTime = tempQue->arr[x].burstTime;
				tempRRque[processesProcessed].finish = tempRRque2[x].finish;
				tempRRque[processesProcessed].burstTimeRemain = 0;
				tempRRque[processesProcessed].isFinished = 1;
				tempRRque[processesProcessed].setup = tempRRque2[x].setup;
				tempRRque[processesProcessed].procID = tempRRque2[x].procID;
				tempRRque[processesProcessed].start = tempRRque2[x].start;
				// updating the values
				currentPos++;
				processesProcessed++;
				// we update the timeToUse which will update currentTime
				// depending on the time the process needed to end execution
				timeToUse = tempRRque2[x].burstTimeRemain; 
			}
			// if the process is too long to be finished in this execution
			// we will only execute the amount of time this queue allows
			// and we put it in the order of processes executed  queue (tempProc) but not the
			// processed processes queue (tempRRque)
			else if (tempRRque2[x].burstTimeRemain > tempQue->timeQuantum){
				tempRRque2[x].burstTimeRemain -= tempQue->timeQuantum;
				tempProc[currentPos] = tempQue->arr[x];
				currentPos++;
				timeToUse = tempQue->timeQuantum;
			}
			// update the time accordingly
			currentTime += timeToUse;
		}
	}

	// this is very similar to FCFS and SJF
	for(int x = 0; x < currentPos; x++){
		if(x != currentPos-1)
			fprintf(fp, "p%d ", tempProc[x].id);
		else
			fprintf(fp, "p%d\n", tempProc[x].id);
	}

	// this segment is different as we don't calculate waiting time
	// but rather turnaround time
	fprintf(fp, "\nTurnaround times for each process:\n");
	int totalWait = 0;

	for(int y = 0; y< processesProcessed; y++){
		fprintf(fp, "p%d = %d\n", tempRRque[y].procID,  tempRRque[y].finish-tempRRque[y].start);
	}

	fprintf(fp, "\n");
	fclose(fp);

}


/**

	the main function performs the simulation of the cpu scheduling algorithms and prints it to the screen
*/
int main(int argc, char* argv){
	// clearing the file from previous work by opeing it for write and closing it
	FILE * file = fopen("cpu_scheduling_output_file.txt", "w");
	fclose(file);

	// setting up the queueTable with the openFile funciton
	openFile("cpu_scheduling_input_file.txt");

	// going through a while loop and having each queue go through each algorithm and storing it to the file
	int x = 0;
	for(; x < lineNumber; x++){
		FCFS(queueTable[x]);
		SJF(queueTable[x]);
		RR(queueTable[x]);
	}

	// opening the out file and printing its contents to the screen
	FILE * fp = fopen(outfile, "r");

	char character = '\0';
	while((character = fgetc(fp)) != EOF)
      		printf("%c", character);

	fclose(fp);

}
