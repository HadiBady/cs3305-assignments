/**
	Developed by Hadi Badawi
	for course CS3305a
	shows difference between fork vs. thread usage
*/

#include<pthread.h>
#include<stdlib.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<errno.h>

// variables values to be used later in calculations
int x = 10, y = 20, z = 0;

// the do_work funciton does the work of calcuating x + y and stores it in z
void *do_work() {
	z = x + y;
	return NULL;
}


/**
	purpose of this function is to show the difference
	between using a fork and a thread to do calculations
*/
int main(int argc, char *argv) {


	// creating the child process
	pid_t child1 = fork();

	// shows error that child could not be created
	if (child1 < 0){
		printf("Could not create child");
		exit(1);
	}
	// if the process is current the child, then do the arithemtic calculation of x + y and store it in z
	else if (child1 == 0){
		z = x + y;
		exit(0);
	}
	// for parent process wait until child is done then print the value of z
	else if (child1 > 0){
		wait(NULL);
		printf("Value of z from fork calculation is: %d\n", z);
	}
	// creating the thread for execution
	pthread_t worker_thread;
	// if not successful with thread execution we print the error message and exit
	if (pthread_create(&worker_thread, NULL, do_work, NULL)) {
		printf("Error while creating thread\n");
		exit(1);
	}

	//we wait for thread to finish by joining our program back
	pthread_join(worker_thread, NULL);

	// print the calculated value of z
	printf("Value of z from thread calculation is: %d\n", z);

}


