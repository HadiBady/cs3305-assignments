/*
	Developed by Hadi Badawi
	The main task of the program is to complete part 1 of Assignment 1
*/

#include<stdlib.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<errno.h>

/** only require a main function which returns 0 when program successfully runs
*/

int main(){

	// creating a fork of the main program to create a parent process
	pid_t parent = fork();

	// if the parent  variable is equal to zero, then we have successfully created
	// the parent process which will create the children
	if (parent == 0){

		// creating the child process
		pid_t child1 = fork();

		// if we are the parent, then we will wait until child 1 is complete
		// before moving on to creating child 2
		if (child1 > 0) {
			wait(NULL);
		}

		// if child creation is successful, then we perform tasks of child 1
		else if (child1 == 0){

			// the following print statements where placed here to have access
			// to both the parent and child pids
			printf("parent process (PID %d) created child_1 (PID %d)\n",
			getppid(), getpid());
			printf("parent process (PID %d) is waiting for child_1 (PID %d) to complete before creating child_2\n",getppid(), getpid());

			// child1 is creating another child to complete its task
			pid_t child1_1 = fork();

			// condition for when child1_1 is created to perform which is
			// just stating successful creation of child_1.1
			if (child1_1 == 0) {
				printf("child_1 (PID %d) created child_1.1(PID %d)\n",
				getppid(), getpid());
				// this is to ensure that child_1.1 does not continue executing
				exit(0);
			}
			// condition for the child_1  to perform the required tasks
			else if (child1_1 > 0){
				wait(NULL);
				printf("child_1 (PID %d) is now completed\n", getpid());
				// again to ensure that program actually terminates
				exit(0);
			}
		}

		// creation of child2 which is done after child1 and child1.1 are created and
		// destroyed
		pid_t child2 = fork();

		// if we are in the child_2 then we need to execute the external program
		// "exterinal program.o"
		if (child2 == 0) {
			printf("parent (PID %d) created child_2 (PID %d)\n", getppid(), getpid());
			printf("child_2 (PID %d) is calling an external program external_program.out and leaving child_2 ...\n", getpid());			execl("external_program.out", NULL);
		}
		// this was added as for some reason the code would use different values
		// for pid and ppid if the wait(NULL) was not used
		else if (child2 > 0){
			wait(NULL);
		}
	}
}
