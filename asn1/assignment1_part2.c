/**
	Developed by Hadi Badawi
	This software will complete Part 2 of Assignment 1

*/

#include<stdlib.h>
#include<stdio.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>

/**
	main function which will execute all the code,
	it will also accept parameters from the terminal for values  of 
	X and Y
*/

int main(int argc, char *argv[] ) {
	// creating parent proceess by forking current process
	pid_t parent = fork();

	// code to execute in parent process
	if (parent == 0) {

		// creating array for the pipe function call
		int fd[2];
		// creating a pipe
		pipe(fd);

		printf("A pipe is created for communication between parent (PID %d) and child\n", getpid());

		// creation of the child process by forking
		pid_t child = fork();

		// condition for the child if successfully created will execute
		if (child == 0){
			// can place this print statement here since this code is only executed
			// if the child was successfully created
			printf("parent (PID %d) created a child (PID %d)\n", getppid(), getpid());

			//  memmory allocating tempNumber which will be a string that stores
			// a copy of the second argument
                        char* tempNumber = (char *) malloc(strlen(argv[2])+1);
			// the second argument is copyed to tempNumber
                        tempNumber = strcpy(tempNumber, argv[2]);

			wait(NULL);
			// We display what we have read from argument 2
                        printf("child (PID %d) Reading Y = %s from the user\n", getpid(), tempNumber);
			// we write what we read into the pipe 
                        write(fd[1], tempNumber, strlen(tempNumber)+1);
                        printf("child (PID %d) Writing Y into the pipe\n", getpid());
			// we free the tempNumber
			free(tempNumber);
			
			
                }

		//  condition for parent process to execute
		if (child > 0) {

			// convert the read string into an integer
			int X = atoi(argv[1]);

			printf("parent(PID %d) Reading X = %d from the user\n", getpid(), X);

			// created string array to store the what is read from the pipe
			char tempNum[strlen(argv[2])]; 

			// ensure that child process completes its tasks before moving on
			wait(NULL);

			// reading from the pipe into the temp array
			read(fd[0], tempNum, strlen(argv[2]));
			// also storing a terminating null character at the end of the array
			tempNum[strlen(argv[2])] = '\0';

			printf("parent(PID %d) Reading Y from the pipe(Y = %s)\n", getpid(), tempNum);

			// converting the read number from the pipe into an int
			int Y = atoi(tempNum);

			// calculating the total sum of the 2 values
			int total = X + Y;

			printf("parent(PID %d) adding X + Y = %d\n", getpid(), total);

		}
	}
	else if (parent > 0){
		wait(NULL);
	}
}
